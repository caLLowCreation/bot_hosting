const functions = require('firebase-functions');
const firebase = require('firebase-admin');
const express = require('express');
const engines = require('consolidate');

const serviceAccount = require("./serviceAccountKey.json");

//const backend = require('./backend');
//backend.startup();

firebase.initializeApp({
    credential: firebase.credential.cert(serviceAccount),
    databaseURL: 'https://twitch-api-viewer.firebaseio.com'
});

function getProducts() {
    const ref = firebase.database().ref('products');
    return ref.once('value').then(snap => {
        return snap.val();
    });
}

function getAccessOptions() {
    const ref = firebase.database().ref('access-options');
    return ref.once('value').then(snap => {
        const items = [];
        snap.forEach((childsnap) => {
            const val = childsnap.val();
            for (let i = 0; i < val.length; i++) {
                items.push(val[i] || { allowedTags: [], allowedAttributes: [] });
            }
        });
        return items.map(x => x);
    })
}

function getCommands() {
    const ref = firebase.database().ref('command-configs');
    return ref.once('value').then(snap => {
        const items = [];
        snap.forEach((childsnap) => {
            items.push(childsnap.val());
        });
        return items;
    })
}

function commandAlias(configs, matchValue) {

    const commandNames = configs.commands.map(x => '!' + x.split("(")[0].substr(1).trim()).filter(unique);
    const nameMatches = commandNames.filter(x => x.toLowerCase() == matchValue);
    const isAlias = configs.name.toLowerCase() !== matchValue;
    const success = nameMatches.length > 0;
    const commandName = success ? nameMatches[0] : '';

    return {
        success: success,
        commandName: commandName,
        commandNames: commandNames,
        nameWithQuery: isAlias ? `${configs.name} alias !${commandName}` : configs.name
    };
}

function unique(value, index, self) {
    return self.indexOf(value) === index;
}

const app = express();
app.engine('hbs', engines.handlebars);
app.set('views', './views');
app.set('view engine', 'hbs');

app.get('/', (request, response) => {
    response.render('index', {
        items: [
            { name: 'Commands', description: 'Help for naivebot Twitch chatbot', url: '/commands?accessLevel=2' },
            { name: 'Access Options', description: 'Access options naivebot Twitch chatbot overlay programming', url: '/access-options' },
            { name: 'Voices', description: 'Voices available for naivebot Twitch chatbot', url: '/voices' },
            { name: 'Products', description: 'caLLowCreation\'s products and services', url: '/products' }
        ]
    });
});

app.get('/voices', (request, response) => {
    response.render('voices');
});

app.get('/products', (request, response) => {
    getProducts().then(products => {
        response.render('products', {
            products
        });
    });
});

app.get('/access-options', (request, response) => {
    getAccessOptions().then(items => {
        response.render('access-options', {
            items,
            options: items.map(x => {
                let xtags = 'ALL';
                if (x.allowedTags.length > 0) {
                    xtags = x.allowedTags.map(m => `<${m}>`).join(' ');
                } else if (x.allowedTags) {
                    xtags = 'NONE';
                }

                let tattributes = 'ALL';
                if (Object.keys(x.allowedAttributes).length > 0) {
                    tattributes = JSON.stringify(x.allowedAttributes);
                } else if (x.allowedAttributes) {
                    tattributes = 'NONE';
                }
                return ({
                    allowedTags: xtags,
                    allowedAttributes: `Allowed Attributes: ${tattributes}`
                })
            })
        });
    });
});

app.get('/raw-commands', (request, response) => {
    getCommands().then(commands => response.status(200).json(commands));
});

app.get('/commands', (request, response) => {
    //response.set('Cache-Control', 'public, max-age=300, s-maxage=600');

    const qs = require('querystring');
    const url = request.url.split('?')[1];
    const req_data = qs.parse(url);
    const queryItem = req_data['accessLevel'] || 0
    const accessLevel = Math.min(Math.max((queryItem), 0), 6);

    console.log(`queryItem: ${req_data['accessLevel']}`);
    console.log(`Current Access Level: ${accessLevel}`);

    getCommands().then(commands => {
        const util = require('util');
        response.render('commands', {
            commands: commands
                .map(x => {
                    const games = [];
                    for (const key in x.games) {
                        games.push(x.games[key]);
                    }
                    const capAccessLevelForCooldown = accessLevel > 3 ? 3 : accessLevel;
                    const accessCooldownTime = x.cooldown / capAccessLevelForCooldown;
                    return (x.accessLevel <= accessLevel && x.enabled) ?
                        ({
                            name: x.name,
                            bitcals: x.bitcals,
                            alias: commandAlias(x, x.name).commandNames.join(' | '),
                            description: util.format(x.messages.help, x.name),
                            example: x.messages.example,
                            offline: x.offline ? 'yes' : 'no',
                            cooldown: (accessCooldownTime / 1000).toFixed(0),
                            games: games.map(g => g.enabled ? g.name : null).filter(g => g).join(', ')
                        }) : null
                })
                .filter(x => x),
            accessLevel: accessLevel
        });
    });
});

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
exports.app = functions.https.onRequest(app);
